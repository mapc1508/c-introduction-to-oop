﻿namespace Ch12CarDealer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabelOpel = new System.Windows.Forms.LinkLabel();
            this.linkLabelMercedes = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Stencil", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pick the car:";
            // 
            // linkLabelOpel
            // 
            this.linkLabelOpel.AutoSize = true;
            this.linkLabelOpel.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelOpel.Location = new System.Drawing.Point(16, 35);
            this.linkLabelOpel.Name = "linkLabelOpel";
            this.linkLabelOpel.Size = new System.Drawing.Size(77, 18);
            this.linkLabelOpel.TabIndex = 1;
            this.linkLabelOpel.TabStop = true;
            this.linkLabelOpel.Text = "Opel Astra";
            this.linkLabelOpel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelOpel_LinkClicked);
            // 
            // linkLabelMercedes
            // 
            this.linkLabelMercedes.AutoSize = true;
            this.linkLabelMercedes.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Underline);
            this.linkLabelMercedes.Location = new System.Drawing.Point(16, 57);
            this.linkLabelMercedes.Name = "linkLabelMercedes";
            this.linkLabelMercedes.Size = new System.Drawing.Size(158, 18);
            this.linkLabelMercedes.TabIndex = 2;
            this.linkLabelMercedes.TabStop = true;
            this.linkLabelMercedes.Text = "Mercedes Benz Class A";
            this.linkLabelMercedes.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelMercedes_LinkClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(265, 92);
            this.Controls.Add(this.linkLabelMercedes);
            this.Controls.Add(this.linkLabelOpel);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "CarDealer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabelOpel;
        private System.Windows.Forms.LinkLabel linkLabelMercedes;
    }
}

