﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch12CarDealer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.label1.Font = carFont;
        }

        Font carFont = new Font("Stencil",14.25f, FontStyle.Italic, GraphicsUnit.Point);

        private void linkLabelOpel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormOpelAstra fOpelAstra = new FormOpelAstra();
            fOpelAstra.ShowDialog();
            linkLabelOpel.LinkColor = linkLabelOpel.VisitedLinkColor;
        }

        private void linkLabelMercedes_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormMercedes mercedes = new FormMercedes();
            mercedes.ShowDialog();
            linkLabelMercedes.LinkColor = linkLabelMercedes.VisitedLinkColor;
        }

      

        
    }
}
