﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch13SunshineSubdivision
{
    public partial class SunshineSubdivision : Form
    {
        public SunshineSubdivision()
        {
            InitializeComponent();
            listBox2.Visible = false;
            label3.Visible = false;
       
        }
        public void AddToListBox(params string[] Items)
        {
            foreach (string item in Items)
            {
                listBox2.Items.Add(item);
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            listBox2.Visible = true;
            label3.Visible = false;
            if (listBox1.SelectedItem == "White")
            {
                AddToListBox("black", "red", "green", "dark blue");
            }
            else if (listBox1.SelectedItem == "Gray")
            {
                AddToListBox("black", "white");
            }
            else AddToListBox("white", "dark blue");
            
            
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Visible = false;
            label3.Visible = true;
            label3.Text = "Congratulations, \nYou've made the \nright choice!";
        }
    }
}

/*
5.
The Sunshine Subdivision allows residents to select siding for
their new homes, but only specific trim colors are allowed
with each siding color. Create a Form for Sunshine Subdivision
that allows a user to choose one of three siding colors from a
ListBox—white, gray, or blue. When the user selects a siding
color, the program should display a second ListBox that
contains only the following choices:
 • White siding—black, red, green, or dark blue trim
 • Gray siding—black or white trim
 • Blue siding—white or dark blue trim
 After the user selects a trim color, the program should
display a congratulatory message on a Label  indicating that
the choice is a good one. T e trim ListBox also becomes
invisible. If the user makes a new selection from the siding
ListBox, the congratulatory message is invisible until the
user selects a complementary trim.
*/