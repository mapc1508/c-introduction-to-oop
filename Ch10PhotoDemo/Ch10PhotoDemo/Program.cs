﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch10PhotoDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Photo newOne = new Photo(8, 10);
            //Console.WriteLine(newOne.ToString());

            MattedPhoto secondOne = new MattedPhoto("green",8, 10);
            Console.WriteLine(secondOne.ToString());
        }
    }

    class Photo
    {

        public Photo()
        {
            checkPrice();
        }
        public Photo(double width, double height)
        {
            this.width = width;
            this.height = height;
            checkPrice();
        }
        double width, height;
        protected double price;
        public double Width
        {
            get
            {
                return width;
            }

            set
            {
                width = value;
            }
        }

        public double Height
        {
            get
            {
                return height;
            }

            set
            {
                height = value;
            }
        }
        public double Price
        {
            get
            {
                return price;
            }
        }

        public virtual void checkPrice()
        {
            if (width == 8 && height == 10) this.price = 3.99;
            else if (width == 10 && height == 12) this.price = 5.99;
            else this.price = 9;
        }
        public override string ToString()
        {
            return String.Format(GetType() + "\nWidth: {0} \nHeight: {1}\nPrice: {2}",Width,Height,Price.ToString("c2"));
        }
    }

    class MattedPhoto: Photo
    {

        public MattedPhoto()
        {
            price = price + 10;
        }
        public MattedPhoto(string color) : base(0, 0)
        {
            this.color = color;
            price = price + 10;
        }
        public MattedPhoto(string color, double height, double width)
            : base(height,width)
        {
            this.color = color;
            price = price + 10;
        }
        
        private string color;
        public string Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }





}
