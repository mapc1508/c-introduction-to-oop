﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch11CollegeApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Acceptance("Mirza", 4, 100);
                Acceptance("Mirza", -1, 100);
            }
            
            catch (ArgumentException a)
            {
                Console.WriteLine(a.Message);

            }
           
        }

        static bool Acceptance(string studentName, double GPA, int testScore)
        {
            if ((GPA < 0 || GPA > 4.00) || (testScore < 0 || testScore > 100))
            {
                throw (new ArgumentException("Test score/GPA invalid"));
            }
            else if (GPA < 2.5 || testScore < 75)
            {
                Console.WriteLine("You fail!");
                return false;
            }
            else
            {
                Console.WriteLine("You have passed!");
                return true;
            }
        }
    }

  




}
