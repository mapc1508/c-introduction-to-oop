﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch9DemoJobs
{
    class Program
    {
        static void Main(string[] args)
        {
            Job first = new Job("cleaning", 2.5, 12);
            Job second = new Job("washing", 1.5, 6);
            Job third = first + second;
            Console.WriteLine(third.ToString());
        }
    }

    class Job
    {
        private string description;
        private double timeToComplete;
        private readonly double totalFee;
        private double hourlyRate;

        public Job()
        {
            totalFee = hourlyRate * timeToComplete;
        }

        public Job(string description, double timeToComplete, double hourlyRate)
        {
            this.description = description;
            this.timeToComplete = timeToComplete;
            this.hourlyRate = hourlyRate;
            totalFee = hourlyRate * timeToComplete;
            
        }
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }
             
        public double TimeToComplete
        {
            get 
            {
                return timeToComplete;
            }
            set 
            {
                timeToComplete = value;
                
            }
        }

        public double HourlyRate
        {
            get
            {
                return hourlyRate;
            }
            set
            {
                hourlyRate = value;
            }
        }


        public static Job operator +(Job one, Job two)
        {
            string newDescription = one.description + " and " + two.description;
            double newTime = one.timeToComplete + two.timeToComplete;
            double newHourlyRate = (one.hourlyRate + two.hourlyRate) - 
                (one.hourlyRate + two.hourlyRate) * 0.05;
            return (new Job(newDescription, newTime, newHourlyRate));
        }

        public override string ToString()
        {
            return String.Format("{0} , total fee: {1}", description, totalFee);
        }
        
    }






}



