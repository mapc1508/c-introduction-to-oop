﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch12LetsMakeADeal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[] pictureArray = {@"C:\Users\Mirza\Desktop\file1049954.jpg",@"C:\Users\Mirza\Desktop\Samsung-laptop2.jpg",
                                 @"C:\Users\Mirza\Desktop\33cb9c848.jpg"};
        int x, y, z;
        bool buttonClicked = false;
        bool buttonClickedTwice = false;

        string decision = "Are you going to change your mind and choose alternative?";
  
        private void RandomNumbers(out int x, out int y, out int z)
        {
            Random randomNumberGenerator = new Random();
            x = randomNumberGenerator.Next(0, 3);

            y = randomNumberGenerator.Next(0, 3);
            while (x == y) y = randomNumberGenerator.Next(0, 3);

            z = randomNumberGenerator.Next(0, 3);
            while (z == x || z == y) z = randomNumberGenerator.Next(0, 3);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RandomNumbers(out x, out y, out z);
            pictureBox1.Image = Image.FromFile(pictureArray[x]);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Visible = false;
            
            pictureBox2.Image = Image.FromFile(pictureArray[y]);
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.Visible = false;
            
            pictureBox3.Image = Image.FromFile(pictureArray[z]);
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.Visible = false;

            //label2.Visible = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!buttonClicked)
            {
                if (x == 0)
                {
                    if (y > z) { pictureBox2.Visible = true; button2.Visible = false; }
                    else { pictureBox3.Visible = true; button3.Visible = false; }
                }
                else if (x != 0)
                {
                    if (y == 0) { pictureBox3.Visible = true; button3.Visible = false; }
                    else { pictureBox2.Visible = true; button2.Visible = false; }
                }
                label2.Text = decision;
            }
            else
            {
                if (!buttonClickedTwice)
                {
                    pictureBox1.Visible = true;
                    button1.Visible = false;
                    label2.Text = "You have chosen first price!";
                    buttonClickedTwice = true;
                }
                else
                {
                    pictureBox1.Visible = true;
                    button1.Visible = false;
                }
            }
            buttonClicked = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!buttonClicked)
            {
                if (y == 0)
                {
                    if (x > z) { pictureBox1.Visible = true; button1.Visible = false; }
                    else { pictureBox3.Visible = true; button3.Visible = false; }
                }
                else if (y != 0)
                {
                    if (x == 0) { pictureBox3.Visible = true; button3.Visible = false; }
                    else { pictureBox1.Visible = true; button1.Visible = false; }
                }
                label2.Text = decision;
            }
            else
            {
                if (!buttonClickedTwice)
                {
                    pictureBox2.Visible = true;
                    button2.Visible = false;
                    label2.Text = "You have chosen second price!";
                    buttonClickedTwice = true;
                }
                else
                {
                    pictureBox2.Visible = true;
                    button2.Visible = false;
                }
            }
            buttonClicked = true;
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!buttonClicked)
            {
                if (z == 0)
                {
                    if (x > y) { pictureBox1.Visible = true; button1.Visible = false; }
                    else { pictureBox2.Visible = true; button2.Visible = false; }
                }
                else if (z != 0)
                {
                    if (y == 0) { pictureBox1.Visible = true; button1.Visible = false; }
                    else { pictureBox2.Visible = true; button2.Visible = false; }
                }

                label2.Text = decision;
            }
            else
            {
                if (!buttonClickedTwice)
                {
                    pictureBox3.Visible = true;
                    button3.Visible = false;
                    label2.Text = "You have chosen third price!";
                    buttonClickedTwice = true;
                }
                else
                {
                    pictureBox3.Visible = true;
                    button3.Visible = false;
                }
            }
            buttonClicked = true;
        }

    }
}
