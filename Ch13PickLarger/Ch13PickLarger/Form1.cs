﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch13PickLarger
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            for (int i = 0; i < firstArray.Length; ++i)
            {
                firstArray[i] = randomNumberGenerator.Next(0, 101);
                secondArray[i] = randomNumberGenerator.Next(0, 101);
            }
        }
        
        Random randomNumberGenerator = new Random();
        int[] firstArray = new int[100];
        int[] secondArray = new int[100];
        int numOfHits = 0, numOfMisses = 0, i = 0;

        private void Button1And2(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b == button1)
                if (firstArray[i] > secondArray[i]) ++numOfHits;
                else ++numOfMisses;
            else if(b == button2)
                if (secondArray[i] > firstArray[i]) ++numOfHits;
                else ++numOfMisses;
            label2.Visible = true;
            panel1.Visible = false;
            label2.Text = String.Format("Numbers compared are {0} and {1}.\nNumber of hits: {2}. Number of misses: {3}",
                firstArray[i], secondArray[i], numOfHits, numOfMisses);
        }      

        private void button3_Click(object sender, EventArgs e)
        {
            if (i == firstArray.Length - 1) i = 0;
            ++i;
            label2.Visible = false;
            panel1.Visible = true;
        }    
    }
}

/*
Create a Form that contains two randomly generated arrays,
each containing 100 numbers. Include two Buttons labeled
“1” and “2”. Starting with position 0 in each array, ask the user
to guess which of the two arrays contains the higher number
and to click one of the two buttons to indicate the guess. After
each button click, the program displays the values of the two
compared numbers, as well as running counts of the number of correct and incorrect guesses. After the user makes a
guess, disable the Buttons while the user views the results.
After clicking a Next Button, the user can make another guess
using the next two array values. If the user makes more than
100 guesses, the program should reset the array subscript to 0
so the comparisons start over, but continue to keep a running
score. Save the project as PickLarger.
*/