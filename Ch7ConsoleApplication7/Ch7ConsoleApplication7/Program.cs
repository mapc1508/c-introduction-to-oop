﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7ConsoleApplication7
{
    class Program
    {
        static void Main(string[] args)
        {
            ShowWelcomeMessage();
        }
        private static void ShowWelcomeMessage()
        {
            Console.WriteLine("Welcome.");
            Console.WriteLine("Have fun!");
            Console.WriteLine("Enjoy the program!");
        }
    }
}
