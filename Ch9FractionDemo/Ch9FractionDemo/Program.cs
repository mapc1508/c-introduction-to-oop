﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch9FractionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Fraction newFrac = new Fraction(9, 16);
            Fraction newFrac2 = new Fraction(16, 9);
            Fraction newFrac3 = newFrac + newFrac2;
            Console.WriteLine(newFrac3.ToString());
            Fraction newFrac4 = newFrac * newFrac2;
            Console.WriteLine(newFrac4.ToString());
        }
    }

    class Fraction
    {
        public Fraction(int wholeNum, int numer, int denom)
        {
            WholeNumber = wholeNum;
            Numerator = numer;
            Denominator = denom;
            Normalize();
        }
        public Fraction(int numer, int denom)
        {
            Numerator = numer;
            Denominator = denom;
            Normalize();
        }
        public Fraction()
        {
            Denominator = 1;
            Normalize();
        }
        private int denominator, numerator;
        public int WholeNumber { get; set; }
        public int Numerator
        {
            get
            {
                return numerator;
            }
            set
            {
                numerator = value;
            }
        }

        public int Denominator
        {
            get
            {
                return denominator;
            }
            set
            {
                if (value != 0)
                {
                    denominator = value;
                }
                else throw (new DivideByZeroException());
            }
        }

        private static int GCD(int a, int b)
        {
            int r = 1;
            while (r != 0)
            {
                r = a % b;
                a = b;
                b = r;
            }
            return a;
        }
        private void Reduce()
        {
            int x = GCD(Numerator, Denominator);
            Numerator = Numerator / x;
            Denominator = Denominator / x;
        }
        public void Normalize()
        {
            Reduce();
            WholeNumber = Numerator / Denominator;
            Numerator = Numerator % Denominator;

        }
        public void Revert()
        {
            Numerator = Numerator + (WholeNumber * Denominator);
            WholeNumber = 0;
        }

        public static Fraction operator +(Fraction one, Fraction two)
        {
            one.Revert();
            two.Revert();

            int LCM = (one.Denominator * two.Denominator) / GCD(one.Denominator, two.Denominator);

            int newNumerator = (LCM / one.Denominator) * one.Numerator;
            int newDenominator = (LCM / two.Denominator) * two.Numerator;

            return new Fraction(newNumerator + newDenominator, LCM);
        }
        public static Fraction operator *(Fraction one, Fraction two)
        {
            one.Revert(); two.Revert();
            int newNumerator = one.Numerator * two.Numerator;
            int newDenominator = one.Denominator * two.Denominator;
            return (new Fraction(newNumerator, newDenominator));
        }
        public override string ToString()
        {
            //Normalize();
            if (WholeNumber != 0)
            {
                if (numerator == 0) return string.Format("{0}", WholeNumber);
                else return string.Format("{0} {1}/{2}", WholeNumber, Numerator, Denominator);
            }
            else
            {
                if (Numerator % Denominator == 0 && numerator != 0) return string.Format("{0}", Numerator/Denominator);
                    else return string.Format("{0}/{1}", Numerator, Denominator);
            }
        }
    }
}