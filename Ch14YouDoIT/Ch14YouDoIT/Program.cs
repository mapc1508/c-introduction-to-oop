﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Ch14YouDoIT
{
    class Program
    {
        static void Main(string[] args)
        {
            //FileStream file = new FileStream("Names.txt", FileMode.Create, FileAccess.Write);
            //StreamWriter writer = new StreamWriter(file);
            //string[] names = {"Anthony ","Belle ","Carolyn ","David ","Edwin ","Frannie ","Gina ","Hannah ","Inez ","Juan "};
            //foreach (string name in names)
            //{
            //    writer.WriteLine(name);
            //}
            //writer.Close();
            //file.Close();

            FileStream file1 = new FileStream("Names.txt", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(file1);
            //string input = reader.ReadLine();
            //while (input != null)
            //{
            //    Console.WriteLine(input);
            //    input = reader.ReadLine();
            //}
            //reader.Close();
            //file1.Close();
            const int END = 999;
            int count = 0;
            int num;
            int size;
            string name;
            name = reader.ReadLine();
            while (name != null)
            {
                ++count;
                name = reader.ReadLine();
            }
            size = (int)file1.Length / count;
            Console.WriteLine((int)file1.Length);
            Console.WriteLine("With which number do you want to start? ");
            num = Convert.ToInt32(Console.ReadLine());

            while (num != END)
            {
                Console.Write("\nStarting with name " + num + ": ");
                file1.Seek((num - 1) * size, SeekOrigin.Begin);
                name = reader.ReadLine();
                Console.WriteLine(" " + name);
                while(name != null)
                {
                    name = reader.ReadLine();
                    Console.WriteLine("   " + name);
                }
                Console.WriteLine("With which number do you want to start? ");
                num = Convert.ToInt32(Console.ReadLine());
            }
            reader.Close();
            file1.Close();
        }
    }
}
