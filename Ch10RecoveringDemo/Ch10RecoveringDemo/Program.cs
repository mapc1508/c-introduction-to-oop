﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch10RecoveringDemo
{
    class Program
    {
        static void Main(string[] args)
        {

            Patient a = new Patient();
            Nanny b = new Nanny();
            FootballPlayer c = new FootballPlayer();

            Console.WriteLine(a.Recover());
            Console.WriteLine(b.Recover());
            Console.WriteLine(c.Recover());
            
        
        }
    }

    public interface IRecoverable
    {
        string Recover();
    }

    class Patient: IRecoverable
    {
        public string Recover()
        {
            return "I feel good!";
        }
    }

    class Nanny : IRecoverable
    {
        public string Recover()
        {
            return "Kids are fine!";
        }
    }

    class FootballPlayer: IRecoverable
    {

        public string Recover()
        {
            return "I played good!";
        }
    }


}
