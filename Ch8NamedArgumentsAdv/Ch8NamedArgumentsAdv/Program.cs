﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch8NamedArgumentsAdv
{
    class Program
    {
        static void Main(string[] args)
        {
            Closing(); Console.WriteLine();
            Closing("Mike \n");
        }
   
    //private static void Closing()
    //    {
    //        Console.WriteLine("Sincerely,\n  James O'Hara");
    //    }

    //SAME THING:
    //private static void Closing(string name)
    //    {
    //        Console.WriteLine("Sincerely,");
    //        Console.WriteLine(name);
    //    }
    private static void Closing(string name = "James O'Hara")
    {
        Console.WriteLine("Sincerely,");
        Console.WriteLine(name);
    }
    }
}
