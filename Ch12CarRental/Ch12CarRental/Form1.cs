﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch12CarRental
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private const double COMPACT_CHARGE = 19.95;
        private const double STANDARD_CHARGE = 24.95;
        private const double LUXURY_CHARGE = 93;

        double totalCharge = 0;

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void comboBoxDays_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (radioButtonCompact.Checked) totalCharge = COMPACT_CHARGE * Convert.ToDouble(comboBoxDays.SelectedItem);
            else if (radioButtonStandard.Checked) totalCharge = STANDARD_CHARGE * Convert.ToDouble(comboBoxDays.SelectedItem);
            else totalCharge = LUXURY_CHARGE * Convert.ToDouble(comboBoxDays.SelectedItem);

            label3.Text = "Total charge: " + totalCharge.ToString("c2"); 
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            label3.Text = "Total charge: ";
            comboBoxDays.Text = "";
        }

    

    

     

    }
}
