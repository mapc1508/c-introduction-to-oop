﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7DisplaySalesTax2Arguments
{
    class Program
    {
        static void Main(string[] args)
        {
            DisplaySalesTax(17000, 0.1);
        }
   
    private  static  void DisplaySalesTax(double saleAmount, double taxRate)
        {
           double tax = taxRate * saleAmount;
           Console.WriteLine("The tax on {0} at {1} is {2}", saleAmount.ToString("c"),
               taxRate.ToString("p"), tax.ToString("c"));
        }
    
    }
}
