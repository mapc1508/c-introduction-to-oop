﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter6BinarySearchDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] idNumbers = { 122, 167, 204, 219, 345 };
            int x;
            int entryId;
            Console.Write("Enter an employee ID ");
            entryId = Convert.ToInt32(Console.ReadLine());
            x = Array.BinarySearch(idNumbers, entryId);
            if (x < 0)
            {
                Console.WriteLine("ID {0} not found", entryId);
            }
            else Console.WriteLine("ID {0} is found at position {1}", entryId, x); 

        }
    }
}