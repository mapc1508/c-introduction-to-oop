﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch12VideoStore
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        double rentPrice = 0;

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            double selectedItems = listBox1.SelectedItems.Count;

            rentPrice = 2.5 * selectedItems;

            // OR CODE BELOW

            //double rentPrice = 0;

            //for (int i = 0; i < listBox1.Items.Count; ++i)
            //{
            //    if (listBox1.GetSelected(i)) rentPrice = rentPrice + 2.5;
                
            //}
            
            label2.Text = "Your price is: " + rentPrice.ToString("c2");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
        }
    }
}
