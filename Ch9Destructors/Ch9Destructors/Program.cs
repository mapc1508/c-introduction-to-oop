﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch9Destructors
{
    class Employee
    {
        public int IdNumber { get; set; }

        public Employee(int empID)
        {
            IdNumber = empID;
            Console.WriteLine("Employee object {0} created", IdNumber);
        }
        ~Employee()
        {
            Console.WriteLine("Employee object {0} destroyed!", IdNumber);
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Employee first = new Employee(1);
            Employee second = new Employee(10);
        
        }
    }
}
