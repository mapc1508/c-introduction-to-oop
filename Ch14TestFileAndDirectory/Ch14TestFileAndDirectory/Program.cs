﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
/*
 Create a program that allows a user to continually enter
directory names until the user types “end”. If the directory
name exists, display a list of the files in it; otherwise, display
a message indicating the directory does not exist. If the
directory exists and files are listed, prompt the user to enter
one of the filenames. If the file exists, display its creation date
and time; otherwise, display a message indicating the file does
not exist. Save the program as TestFileAndDirectory.cs.
Create as many test directories and files as necessary to test
your program. 
 */
namespace Ch14TestFileAndDirectory
{
    class Program
    {
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(@"D:\Torrent\Movies");
            const string END = "END";
            string fileTyped;
            Console.Write("Type the name of the directory or END to exit: ");
            string directory = Console.ReadLine();
            while (directory != END)
            {
                if (Directory.Exists(directory))
                {
                    foreach (string file in Directory.GetFiles(directory))
                    {
                        Console.WriteLine(file);
                    }
                    Console.Write("Enter one of the file names: ");
                    fileTyped = Console.ReadLine();
                    fileTyped =  @"D:\Torrent\Movies\" + @directory + @"\" + @fileTyped;
                    Console.WriteLine(fileTyped);
                    if (File.Exists(fileTyped))
                    {
                        Console.WriteLine("File was created on {0}", File.GetCreationTime(fileTyped));
                    }
                    else Console.WriteLine("File does not exist");
                }
                else Console.WriteLine("The directory does not exist.");

                Console.Write("Type the name of the directory or END to exit: ");
                directory = Console.ReadLine();
            }
        }
    }
}
