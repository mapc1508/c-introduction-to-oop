﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch4MovieDiscountForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int age;
            char rating;
            const int CHILD_AGE = 12;
            const int SENIOR_AGE = 65;
            age = Convert.ToInt32(textBox1.Text);
            rating = Convert.ToChar(textBox2.Text);
            label4.Text = String.Format("When age is {0} and rating is {1}",
            age, rating);
            if ((age <= CHILD_AGE || age >= SENIOR_AGE) && rating == 'G')
                label4.Text += "\nDiscount applies";
            else
               label4.Text += "\nFull price";   
        }

        
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
