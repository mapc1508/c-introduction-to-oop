﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch12NinasCookieSource
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();           
        }

        private const double THIN_MINTS_PRICE = 5;
        private const double SAMOAS_PRICE = 7;
        private const double TAGALONGS_PRICE = 5.5;

        double price = 0;
        double changedPrice;

        private void button1_Click(object sender, EventArgs e)
        {

            if (radioButtonHalfDozen.Checked) changedPrice = 0.5;

            else if (radioButtonDozen.Checked) changedPrice = 1;

            else if (radioButton2Dozen.Checked) changedPrice = 2;

            else if (radioButton3Dozen.Checked) changedPrice = 3;

            if (radioButtonThinMints.Checked)
                price = THIN_MINTS_PRICE * changedPrice;

            else if (radioButtonSamoas.Checked)
                price = SAMOAS_PRICE * changedPrice;

            else if (radioButtonTagalongs.Checked)
                price = TAGALONGS_PRICE * changedPrice;

            label5.Text = "Price is: " + price.ToString("c2");
            label4.Text = "Delivery date: " + monthCalendar1.SelectionStart.AddDays(3).ToString("MMMM dd, yyyy");


        }

      

        
    }
}
