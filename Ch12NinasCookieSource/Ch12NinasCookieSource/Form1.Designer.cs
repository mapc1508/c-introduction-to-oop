﻿namespace Ch12NinasCookieSource
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonThinMints = new System.Windows.Forms.RadioButton();
            this.radioButtonSamoas = new System.Windows.Forms.RadioButton();
            this.radioButtonTagalongs = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonHalfDozen = new System.Windows.Forms.RadioButton();
            this.radioButtonDozen = new System.Windows.Forms.RadioButton();
            this.radioButton2Dozen = new System.Windows.Forms.RadioButton();
            this.radioButton3Dozen = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select your cookie:";
            // 
            // radioButtonThinMints
            // 
            this.radioButtonThinMints.AutoSize = true;
            this.radioButtonThinMints.Location = new System.Drawing.Point(6, 38);
            this.radioButtonThinMints.Name = "radioButtonThinMints";
            this.radioButtonThinMints.Size = new System.Drawing.Size(74, 17);
            this.radioButtonThinMints.TabIndex = 1;
            this.radioButtonThinMints.TabStop = true;
            this.radioButtonThinMints.Text = "Thin Mints";
            this.radioButtonThinMints.UseVisualStyleBackColor = true;
            // 
            // radioButtonSamoas
            // 
            this.radioButtonSamoas.AutoSize = true;
            this.radioButtonSamoas.Location = new System.Drawing.Point(6, 62);
            this.radioButtonSamoas.Name = "radioButtonSamoas";
            this.radioButtonSamoas.Size = new System.Drawing.Size(63, 17);
            this.radioButtonSamoas.TabIndex = 2;
            this.radioButtonSamoas.TabStop = true;
            this.radioButtonSamoas.Text = "Samoas";
            this.radioButtonSamoas.UseVisualStyleBackColor = true;
            // 
            // radioButtonTagalongs
            // 
            this.radioButtonTagalongs.AutoSize = true;
            this.radioButtonTagalongs.Location = new System.Drawing.Point(6, 85);
            this.radioButtonTagalongs.Name = "radioButtonTagalongs";
            this.radioButtonTagalongs.Size = new System.Drawing.Size(75, 17);
            this.radioButtonTagalongs.TabIndex = 3;
            this.radioButtonTagalongs.TabStop = true;
            this.radioButtonTagalongs.Text = "Tagalongs";
            this.radioButtonTagalongs.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Select amount:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.radioButtonThinMints);
            this.panel1.Controls.Add(this.radioButtonTagalongs);
            this.panel1.Controls.Add(this.radioButtonSamoas);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(118, 117);
            this.panel1.TabIndex = 5;
            // 
            // radioButtonHalfDozen
            // 
            this.radioButtonHalfDozen.AutoSize = true;
            this.radioButtonHalfDozen.Location = new System.Drawing.Point(15, 39);
            this.radioButtonHalfDozen.Name = "radioButtonHalfDozen";
            this.radioButtonHalfDozen.Size = new System.Drawing.Size(79, 17);
            this.radioButtonHalfDozen.TabIndex = 6;
            this.radioButtonHalfDozen.TabStop = true;
            this.radioButtonHalfDozen.Text = "1/2 dozens";
            this.radioButtonHalfDozen.UseVisualStyleBackColor = true;
            // 
            // radioButtonDozen
            // 
            this.radioButtonDozen.AutoSize = true;
            this.radioButtonDozen.Checked = true;
            this.radioButtonDozen.Location = new System.Drawing.Point(15, 63);
            this.radioButtonDozen.Name = "radioButtonDozen";
            this.radioButtonDozen.Size = new System.Drawing.Size(63, 17);
            this.radioButtonDozen.TabIndex = 7;
            this.radioButtonDozen.TabStop = true;
            this.radioButtonDozen.Text = "1 dozen";
            this.radioButtonDozen.UseVisualStyleBackColor = true;
            // 
            // radioButton2Dozen
            // 
            this.radioButton2Dozen.AutoSize = true;
            this.radioButton2Dozen.Location = new System.Drawing.Point(15, 87);
            this.radioButton2Dozen.Name = "radioButton2Dozen";
            this.radioButton2Dozen.Size = new System.Drawing.Size(68, 17);
            this.radioButton2Dozen.TabIndex = 8;
            this.radioButton2Dozen.TabStop = true;
            this.radioButton2Dozen.Text = "2 dozens";
            this.radioButton2Dozen.UseVisualStyleBackColor = true;
            // 
            // radioButton3Dozen
            // 
            this.radioButton3Dozen.AutoSize = true;
            this.radioButton3Dozen.Location = new System.Drawing.Point(15, 111);
            this.radioButton3Dozen.Name = "radioButton3Dozen";
            this.radioButton3Dozen.Size = new System.Drawing.Size(68, 17);
            this.radioButton3Dozen.TabIndex = 9;
            this.radioButton3Dozen.TabStop = true;
            this.radioButton3Dozen.Text = "3 dozens";
            this.radioButton3Dozen.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.radioButton3Dozen);
            this.panel2.Controls.Add(this.radioButtonHalfDozen);
            this.panel2.Controls.Add(this.radioButton2Dozen);
            this.panel2.Controls.Add(this.radioButtonDozen);
            this.panel2.Location = new System.Drawing.Point(12, 135);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(107, 145);
            this.panel2.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(161, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Choose order date:";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(164, 50);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(164, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Delivery date: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(164, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Price is: ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(343, 257);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 290);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonThinMints;
        private System.Windows.Forms.RadioButton radioButtonSamoas;
        private System.Windows.Forms.RadioButton radioButtonTagalongs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonHalfDozen;
        private System.Windows.Forms.RadioButton radioButtonDozen;
        private System.Windows.Forms.RadioButton radioButton2Dozen;
        private System.Windows.Forms.RadioButton radioButton3Dozen;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
    }
}

