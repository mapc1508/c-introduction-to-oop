﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch8TryParse
{
    class Program
    {
        static void Main(string[] args)
        {
            string entryString;
            int score;
            Console.Write("Enter your test score >> ");
            entryString = Console.ReadLine();
            int.TryParse(entryString, out score);
            Console.WriteLine("You entered {0}", score);

        
        }
    }
}
