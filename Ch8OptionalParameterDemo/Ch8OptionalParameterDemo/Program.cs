﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch8OptionalParameterDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Using 2 arguments: ");
            DisplaySize(4, 6);

            Console.Write("Using 3 arguments: ");
            DisplaySize(4, 6, 8);
        
        }

    private static void DisplaySize(int length, int width, int height = 1)
        {
            int area = length * width * height;
            Console.WriteLine("Size is {0}", area);
        }
    }
}
