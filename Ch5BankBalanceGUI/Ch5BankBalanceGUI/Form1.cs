﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch5BankBalanceGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double bankBal = 1000;
        const double INT_RATE = 0.04;
        
        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text += String.Format("Bank balance is {0}\n", bankBal.ToString("C"));
            bankBal = bankBal + bankBal * INT_RATE;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label2.Text = "Have a nice day!";
        }
    }
}
