﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch11BookExceptionDemo
{
    class Program
    {        
        static void Main(string[] args)
        {
            Book[] bookArray = new Book[3];
            int i=0;
            try
            {
                for (i=0; i < bookArray.Length; ++i)
                {
                    Console.WriteLine("Enter book's title: ");
                    string title = Console.ReadLine();
                    Console.WriteLine("Enter book's author: ");
                    string author = Console.ReadLine();
                    Console.WriteLine("Enter number of pages: ");
                    int numberOfPages = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter price of the book: ");
                    double price = Convert.ToDouble(Console.ReadLine());
                    bookArray[i] = new Book(title, author, numberOfPages, price);

                }

            }
            catch (BookException e)
            {
                Console.WriteLine(e.Message);
            }
  
        }
    }
    
    class BookException: Exception
    {
        
        public BookException(string title,double price,int numOfPages): 
            base(String.Format("For {0}, ratio is invalid.\n...Price is {1} for {2} pages",
                title, price.ToString("c2"), numOfPages)) { }
    }

    class Book
    {

        public Book(string title, string author, int numOfPages, double price)
        {
            this.Title = title;
            this.Author = author;
            this.NumOfPages = numOfPages;
            this.Price = price; 
        }
        private string title, author;
        double price;
        int numOfPages;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string Author
        {
            get { return author; }
            set { author = value; }
        }
        public double Price
        {
            get 
            { 
                return price; 
            }
            set 
            {
                price = value;
                if (price > 0.1 * numOfPages) throw (new BookException(title, price, numOfPages));
            }
        }
        public int NumOfPages
        {
            get { return numOfPages; }
            set { numOfPages = value; }
        }

        public override string ToString()
        {
            return String.Format("Book: {0}\nAuthor: {1}\nNumber of pages: {2}\nPrice: {3}",
                Title, Author, NumOfPages, Price.ToString("c2"));
        }
    }







}
