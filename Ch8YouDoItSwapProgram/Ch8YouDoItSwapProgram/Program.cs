﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch8YouDoItSwapProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            int first; int second;
            //Console.Write("Before swap first is {0}  and second is {1}\n", first, second);
            Swap(out first, out second);
            //Console.WriteLine("After  swap first is {0} and second is {1}", first, second);


        }
    
    private static void Swap(out int one, out int two)
        {
            one = int.Parse(Console.ReadLine());
            two = int.Parse(Console.ReadLine());
            Console.Write("Before swap first is {0}  and second is {1}\n", one, two);
            int temporary;
            temporary = one;
            one = two;
            two = temporary;
            Console.WriteLine("After  swap first is {0} and second is {1}", one, two);
        }
    
    }
}
