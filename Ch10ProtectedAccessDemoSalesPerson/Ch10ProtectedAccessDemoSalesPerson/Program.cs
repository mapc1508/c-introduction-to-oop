﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch10ProtectedAccessDemoSalesPerson
{
   class  Employee
{
private  int  empNum;
protected  double  empSal;

       public  int  EmpNum {get; set;}
       public double EmpSal
       {
           get
           {
               return empSal;
           }
           set
           {
               double MINIMUM = 1500;
               if (value < MINIMUM)
                   empSal = MINIMUM;
               else empSal = value;
           }
       }
            public  string  GetGreeting()
        {
        string  greeting  =  "Hello.  I  am  employee  #"  +  EmpNum;
        return  greeting;
        }
   }

   class CommissionEmployee : Employee
   {
       private double commissionRate;
       public double CommissionRate
       {
           get
           {
               return commissionRate;
           }
           set
           {
               commissionRate = value;
               empSal = 0; //accessible because this field has protected access modifier
           }
       }
   }
    class Program
    {
        static void Main(string[] args)
        {
                CommissionEmployee  salesperson  =  new  CommissionEmployee();
                salesperson.EmpNum  =  345;
                salesperson.EmpSal  =  20000;
                salesperson.CommissionRate  =  0.07;
                Console.WriteLine("Salesperson  #{0}  makes  {1}  per  year",
                salesperson.EmpNum,
                salesperson.EmpSal.ToString("C"));
                Console.WriteLine("...plus  {0}  commission  on  all  sales",
                salesperson.CommissionRate.ToString("P"));
        
        
        }
    }
}
