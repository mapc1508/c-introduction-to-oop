﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch9DemoSquares
{
    class Program
    {
        static void Main(string[] args)
        {
            Random randomNum = new Random();
            
            Square[] tenSquares = new Square[10];
            for (int i = 0; i < tenSquares.Length; ++i)
            {
                int random = randomNum.Next(1, 10);
                int random2 = randomNum.Next(1, 10);
                string temp = random.ToString() + "." + random2.ToString();
                double doubleRandom = Convert.ToDouble(temp);
                tenSquares[i] = new Square(doubleRandom);
                Console.WriteLine("Area of square #{0} with side {1} is {2}", i+1, tenSquares[i].Side, tenSquares[i].Area);
            }
       
        }
    }
    class Square
    {
        public Square(double side)
        {
            this.side = side;
            areaCalc(Side);
        }
        private double side;
        private double area;
        public double Side
        {
            get
            {
                return side;
            }      
        }
        public double Area
        {
            get
            {
                return area;
            }
        }

        private double areaCalc(double oneSide)
        {
            area = side * side;
            return area;
        }
    
    
    
    }


}
