﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch15Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 6, 4, 2, 1, 8, 3, 7, 5, 2, 0 };
            var lowInts = from x in numbers where x < 5 select x;
            foreach (var x in lowInts)
            {
                Console.WriteLine(x);
            }
        }
    }
}
