﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch13GuessANumber
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            randomNumber = randomNumberGenerator.Next(0, 5);
            label1.Location = new Point(13, 40);
   
        }
            int randomNumber, hintRandom;
            Random randomNumberGenerator = new Random();
           
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rB = (RadioButton)sender;
            if (rB.TabIndex == randomNumber) label1.Text = "You guessed right!";
            else label1.Text = String.Format("You were wrong, the right choice is {0}", ++randomNumber);
            panel1.Visible = false;
            label3.Text = "";
        }

        private void label3_MouseHover(object sender, EventArgs e)
        {
            do
            {
                hintRandom = randomNumberGenerator.Next(0, 5);
            } while (hintRandom == randomNumber);

            ++hintRandom;
            label3.Text += String.Format("\nWrong choice\nwould be {0}", hintRandom);
        }

    }
}

/*
Create a Form that contains a guessing game with five
RadioButtons numbered 1 through 5. Randomly choose one
of the RadioButtons as the winning button. When the user
clicks a RadioButton, display a message indicating whether
the user is right.
 Add a Label  to the Form that provides a hint. When the
user’s mouse hovers over the label, notify the user of one
RadioButton that is incorrect. After the user makes a
selection, disable all the RadioButtons. Save the project as
GuessANumber.
*/