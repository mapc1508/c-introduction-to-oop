﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch6DemoForEachWithEnum
{
    class Program
    {
       enum Day
       {
           SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
           THURSDAY, FRIDAY, SATURDAY
       }
        static void Main(string[] args)
        {
            foreach (string day in Enum.GetNames(typeof(Day)))
                Console.WriteLine(day);
        }
    }
}
