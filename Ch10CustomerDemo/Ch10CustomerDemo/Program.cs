﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch10CustomerDemo
{
    static class Program
    {
        static void Main(string[] args)
        {
            //Customer[] customersArray = new Customer[5];
            //customerInfo(customersArray);
            CreditCustomer[] creditCustomerArray = new CreditCustomer[5];
            customerInfo(creditCustomerArray);
            CreditCustomer newC = new CreditCustomer();
   
        }
        public static double MonthlyRate(this double number)
        {
            number = number * 1 / 24;
            return number;
        }

        public static void customerInfo(CreditCustomer[] customersArray)
        {
            double totalDue = 0;
            for (int i = 0; i < customersArray.Length; ++i)
            {
                customersArray[i] = new CreditCustomer();
                Console.WriteLine("Enter name for credit customer #{0}: ", i + 1);
                customersArray[i].Name = Convert.ToString(Console.ReadLine());
                Console.WriteLine("Enter credit customer #{0} balance due: ", i + 1);
                customersArray[i].BalanceDue = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter credit customer #{0} monthly interest rate: ", i + 1);
                customersArray[i].MonthlyInterestRate = Convert.ToDouble(Console.ReadLine());
                customersArray[i].MonthlyPaymentDue = customersArray[i].BalanceDue.MonthlyRate();
                
                totalDue += customersArray[i].BalanceDue;
            }

            foreach (CreditCustomer c in customersArray)
            {
                Console.WriteLine(c.ToString() + "\nTotal due: " + totalDue + "\n");
            }
        }

        public static void customerInfo(Customer[] customersArray)
        {
            double totalDue = 0;
            for (int i = 0; i < customersArray.Length; ++i)
            {
                customersArray[i] = new Customer();
                Console.WriteLine("Enter name for customer #{0}: ", i + 1);
                customersArray[i].Name = Convert.ToString(Console.ReadLine());
                Console.WriteLine("Enter customer #{0} balance due: ", i + 1);
                customersArray[i].BalanceDue = Convert.ToDouble(Console.ReadLine());
                totalDue += customersArray[i].BalanceDue;
            }
            
            foreach (Customer c in customersArray)
            {
                Console.WriteLine(c.ToString() + "\nTotal due: " + totalDue + "\n");
            } 
        }
    
    
    }

    class Customer: IComparable
    {
        public int IDNumber { get; set; }
        public string Name { get; set; }
        public double BalanceDue { get; set; }

        int IComparable.CompareTo(object O)
        {
            Customer newCustomer = new Customer();
            O = newCustomer;
            if (this.IDNumber > newCustomer.IDNumber) return 1;
            else if (this.IDNumber < newCustomer.IDNumber) return -1;
            else return 0;
        }

        public override string ToString()
        {
            return string.Format("Customer#{0}\nName: {1}\nBalance Due: {2}", IDNumber,
                Name, BalanceDue);
        }

    }

     class CreditCustomer: Customer
    {
        private double monthlyInterestRate;
        public double MonthlyPaymentDue { get; set; }
        public double MonthlyInterestRate
        {
            get
            {
                return monthlyInterestRate;
                
            }

            set
            {
                monthlyInterestRate = value/100;
            }
        }

        public override string ToString()
        {
            return string.Format(base.ToString() + "\nMonthly interest rate: {0}",
                MonthlyInterestRate.ToString("p") + "\nMonthly payment due: " +
                MonthlyPaymentDue.ToString("c2"));
        }
     
    }




}
