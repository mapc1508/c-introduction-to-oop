﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch9OverloadingConstructors
{

    class Employee
    {

        public int IdNumber { get; set; }
        public double Salary { get; set; }
        public Employee()
        {
            IdNumber = 999;
            Salary = 0;
        }
        public Employee(int empId)
        {
            IdNumber = empId;
            Salary = 0;
        }
        public Employee(int empId, double sal)
        {
            IdNumber = empId;
            Salary = sal;
        }
        public Employee(char code)
        {
            IdNumber = 111;
            Salary = 100000;
        }
    }  
    class Program
    {
        static void Main(string[] args)
        {
            Employee aWorker = new Employee();
            Console.WriteLine("{0,4}{1,14}", aWorker.IdNumber, aWorker.Salary.ToString("C"));

            Employee anotherWorker = new Employee(234);
            Console.WriteLine("{0,4}{1,14}", anotherWorker.IdNumber, anotherWorker.Salary.ToString("C"));

            Employee theBoss = new Employee('A');
            Console.WriteLine("{0,4}{1,14}", theBoss.IdNumber, theBoss.Salary.ToString("C"));
          
            
        
        }
    }
}
