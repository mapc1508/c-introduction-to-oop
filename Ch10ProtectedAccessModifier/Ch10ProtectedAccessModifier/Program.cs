﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch10ProtectedAccessModifier
{
    class Employee
    {
        //private int empNum;
        protected double empSalary;

        public int EmpNum { get; set; }
        public double EmpSal
        {
            get
            {
                return empSalary;
            }
            set
            {
                const double MINIMUM = 1500;
                if (EmpSal < MINIMUM)
                    empSalary = MINIMUM;
                else
                    empSalary = value;
            }
        }
       
        public string GetGreeting()
        {
            string greeting = "Hello.  I  am  employee  #" + EmpNum;
            return greeting;
        }
    }
    
    class  CommissionEmployee  :  Employee
    {
        private double commissionRate;
        public double CommissionRate
        {
            get
            {
                return commissionRate;
            }

            set
            {
                commissionRate = value;
                empSalary = 0;

            } 
       
       }
        new public string GetGreeting()
        {
            string greeting = base.GetGreeting();
            greeting = greeting + "\nI  work  on  commission.";
            return greeting;    
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
     
            CommissionEmployee salesPerson = new CommissionEmployee();
            salesPerson.EmpNum = 345;
            salesPerson.EmpSal = 2000;
            salesPerson.CommissionRate = 0.07;

            Console.WriteLine("Salesperson  #{0}  makes  {1}  per  year",
            salesPerson.EmpNum,
            salesPerson.EmpSal.ToString("C"));

            Console.WriteLine("...plus  {0}  commission  on  all  sales", salesPerson.CommissionRate.ToString("P"));         
        }
        
    }
}
