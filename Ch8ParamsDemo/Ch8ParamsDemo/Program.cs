﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch8ParamsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Mark", "Paulette", "Carol", "James" };
            DisplayStrings("Ginger");
            DisplayStrings("George", "Maria", "Thomas");
            DisplayStrings(names);
        
        }
        private static void DisplayStrings(params  string[] people)
        {
            foreach (string person in people)
                Console.Write("{0} ", person);
            Console.WriteLine("\n — — — — — — — — — — — — — —");
            
        }
    
    }
}
