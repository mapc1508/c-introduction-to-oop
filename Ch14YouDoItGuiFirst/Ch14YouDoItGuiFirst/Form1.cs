﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Ch14YouDoItGuiFirst
{
    public partial class Form1 : Form
    {
        public string DELIM = ",";
        int num;
        string name;
        double amount;
        FileStream file1; 
        StreamWriter writer;
        public Form1()
        {
            InitializeComponent();
            file1 = new FileStream("invoiceFile.txt", FileMode.Create, FileAccess.Write);
            writer = new StreamWriter(file1);
            
 
        }

        private void enterButton_Click(object sender, EventArgs e)
        {
            num = Convert.ToInt32(invoiceBox.Text);
            name = nameBox.Text;
            amount = Convert.ToDouble(amountBox.Text);
            writer.WriteLine(num + DELIM + name + DELIM + amount);
            invoiceBox.Clear();
            nameBox.Clear();
            amountBox.Clear();
        }
    }
}
