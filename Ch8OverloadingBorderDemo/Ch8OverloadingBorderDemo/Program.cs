﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch8OverloadingBorderDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            DisplayWithBorder("Ed");
            DisplayWithBorder("Theodore");
            DisplayWithBorder("Jennifer Ann");
            DisplayWithBorder(1024);
            
            
        }
    
        private  static  void DisplayWithBorder(string word)
        {
            const int EXTRA_STARS = 4;
            const string SYMBOL = "#";
            int size = word.Length + EXTRA_STARS;
            for (int x = 0; x < size; ++x)
                Console.Write(SYMBOL);
            Console.WriteLine();
            Console.WriteLine(SYMBOL + " " + word + " " + SYMBOL);
            for (int x = 0; x < size; ++x)
                Console.Write(SYMBOL);
            Console.WriteLine("\n\n");
           
        }
        
        private  static  void DisplayWithBorder(int number)
        {
            const int EXTRA_STARS = 4;
            const string SYMBOL = "#";
            int size = EXTRA_STARS + 1;
            int leftOver = number;
            int x;
            while (leftOver >= 10)
            {
                leftOver = leftOver / 10;
                ++size;
            }
            for (x = 0; x < size; ++x)
                Console.Write(SYMBOL);
            Console.WriteLine();
            Console.WriteLine(SYMBOL + " " + number + " " + SYMBOL);
            for (x = 0; x < size; ++x)
                Console.Write(SYMBOL);
            Console.WriteLine("\n\n");
        }
    
    }
}
