﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch8ComputeTotalPay
{
    class Program
    {
        static void Main(string[] args)
        {
            double bonus=0;
            double totalPay;
            totalPay = ComputeTotalPay(ComputeGross(50,0.1,ref bonus), bonus );
            Console.WriteLine("Total pay is {0}", totalPay);
        
        }
        private static double ComputeGross(double hours, double rate, ref double bonus)
        {
            double gross = hours * rate;
            if (hours >= 40)
                bonus = 100;
            else
                bonus = 50;
            return gross;

        }
        private static double ComputeTotalPay(double gross, double bonus)
        {
            double total = gross + bonus;
            return total;
        }
    
    
    
    }
}