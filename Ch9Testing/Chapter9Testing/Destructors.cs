﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9Testing
{
    class Destructors
    {
        static void Main()
        {
            Employee one = new Employee(1);
            Employee two = new Employee(5);
        }
    }
    class Employee
    {
        public int EmployeeID { get; set; }
        public Employee(int id)//constructor
        {
            EmployeeID = id;
            Console.WriteLine("Employee #{0} object created", EmployeeID);
        }
        ~Employee() //destructor, instance is destroyed, most often when goes out of scope
        {
            Console.WriteLine("Employee #{0} object destructed", EmployeeID);
        }
    }


}
