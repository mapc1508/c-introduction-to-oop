﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9Testing
{
    class OverloadingOperators
    {
        static void Main()
        {
            Book one = new Book("Great Gatsby",656);
             Book two = new Book("Ana Karenina",897);
            Book three = one + two;
            Console.WriteLine(three.Name + three.Pages);
            one.Price = 500;
            one = -one; //minus is use to retrun a price with a 10% discount

            
            Console.WriteLine("Price is {0}",one.Price); 
        }
    
    }

    /*•Overloadable unary operators:
    + - ! ~ ++ -- true false
    •Overloadable binary operators:
    + - * / % & | ^ == != > < >= <=
    •You cannot overload the following operators:
    = && || ?? ?: checked unchecked new typeof as is
    •You cannot overload an operator for a built-in data type
     Some operators must be overloaded in pairs:
     == with !=, and < with >*/

    class Book
    {
        public Book(string name, int pages)
        {
            Pages = pages;
            Name = name;
        }
        public int Pages { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public static Book operator +(Book one, Book two) // binary operator
        {
            int newPages = one.Pages + two.Pages;
            string newName = one.Name + " and " + two.Name + '\n';
            return (new Book(newName, newPages));
        }

        public static Book operator -(Book one) //unary operator
        {
            one.Price = one.Price - one.Price/10;
             return one;
        }



    }






}
