﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9Testing
{
    class ReadonlyField
    {
        static void Main()
        {
        //    ActualTime rightNow = new ActualTime();
            
        //    Console.WriteLine(rightNow.showTime());    


            int t, a, b;
            a = 2;
            b = 18;
            t = b;
            b = a % b;
            a = t;
            Console.WriteLine(t);

        
        
        
        }
        
        
    
    }

    class ActualTime
    {
        readonly DateTime timeNow;//readonly field, similar to named constant, but can be declared during execution time, cannot be changed
        public ActualTime()
        {
            timeNow = DateTime.Now;
        }
        public DateTime showTime()
        {
            return timeNow;
        }

    }





}
