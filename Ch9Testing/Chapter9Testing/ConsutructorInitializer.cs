﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9Testing
{
    class ConsutructorInitializer
    {
        static void Main()
        {
            Employee officer = new Employee();
            Console.WriteLine(officer.Salary);//shows that  public Employee(double salary) is first called
        }
    }

    class Employee
    {
        public int Id {get;set;}
        public double Salary {get;set;}

        public Employee() :this (99.9) /*Constructor initialzier -> points out that another constructor should be called (based on parameters)
                                        * before body of current is executed*/
        {

        }
        public Employee(double salary)
        {
            Salary = salary;
        }
    }


}
