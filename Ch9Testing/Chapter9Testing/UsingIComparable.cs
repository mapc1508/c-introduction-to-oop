﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9Testing
{
    class UsingIComparable
    {
        static void Main()
        {
            Names Anela = new Names(); Anela.Age = 25;
            Names Mirza = new Names(); Mirza.Age = 20;
            Names[] names = { Anela, Mirza };
            Array.Sort(names);
            for (int i = 0; i < names.Length; ++i)
            {
                Console.WriteLine(names[i].Age);
            }



            
        }
    
    }

    
    class Names:IComparable //have to indicate that you are using an interface with a collon and the name of that interface
    {
        
        
        public int Age { get; set; }

        int IComparable.CompareTo(object o)//objects is a type of class that all classes belong to
        {

            Names newName = new Names();
            o = newName;//have to explicitly parse object o as a class to which it belongs
            if (this.Age > newName.Age) return +1;
            else if (this.Age == newName.Age) return 0;
            else return -1;
        }
    }

        
    }







