﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9Testing
{
    class ThisReference
    {
        static void Main()
        {

        }
    
    }

    class Triangle
    {
        float sideA;
        float sideB;
        float sideC;

        public Triangle(float sideA, float sideB, float sideC) //constructor
        {
            this.sideA = sideA; // this reference connects it to a field
            this.sideB = sideB;
            this.sideC = sideC;

        /* this reference is always implicit and should be used only in ambiguous situations */
        }
            
    }

}
