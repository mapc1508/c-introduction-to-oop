﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9Testing
{
    class ObjectInitializor
    {
        static void Main()
        {
            Triangle one = new Triangle() { SideA = 9};//object initializor,
            Triangle two = new Triangle() { SideB = 4};  /* multiple objects with different initial statements, without using constructors
                                                          * for every possible situation */
            Triangle three = new Triangle() { SideC = 65 };


            int result = 16 % 32;
            Console.WriteLine(result);
        }
    
    }

    class Triangle
    {
        public int SideA { get; set;}
        public int SideB { get; set; }
        public int SideC { get; set; }

        public Triangle() //object initializors use default constructors
        {
            SideA = 6;
            SideB = 12;
            SideC = 8;
        }

        public Triangle(int a) //an example of using multiple constructors
        {
            SideA = 9;
        }
    }




}
