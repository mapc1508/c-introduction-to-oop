﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter9Testing
{
    class CreatingClass //application class, contains Main() method
        //this is also a client class -> instantiates objects of another class
    {
        static void Main() 
        {
            Employee first = new Employee(); // creation of an object, identifier and type used, memory allocated
            Console.WriteLine(first.IdNumber);
            first.WelcomeMessage(); //calling instance method that is public 
        }
    }

    class Employee //access modifier can be: private, public, protected, internal(default) // class for instantiation of objects
    {

        public Employee() //constructor with no parameters
            //default constructor is auto-implemented and has no parameters
        {
            PayRate = 15.2;
        }
        public Employee(double rate) //constructor with one parameter
        {
            PayRate = rate;
        }
        //constructor are overloaded, and can use numerous of them, unless you cause ambiguity
        private int idNumber; // instance variable(field), most commonly private - information hiding
        //sometimes fields are public and methods private
        public double PayRate { get; set; }
        public static string MOTTO = "I am an experienced employee"; //-public field, named constant, always static -> same for every instance  
        public int IdNumber //this is a property, it has a get and set accessor
        {
            get //returns given value
            {
                return idNumber = 5; //if a property has only get accessor it is read-only
            }
            set //sets idNumber field
            {
                idNumber = value; //value is a contextial keyword -> only in certain circumstances
            }

            /* public int IdNumber {get; set;} -this is a auto-implemented property, it doesn't depend on class */
        }
        public void WelcomeMessage() //instance methods are most commonly public (private data/public methods)
            //both instance variables and methods are non-static, they belong to a class
        {
            Console.WriteLine("Welcome employee #{0}\nHow can I help you?", idNumber);
        } 
     //if another object is placed here it would be called composition
    
    }
    // static void showInfo(string sth, Employee first) -> objects can be passed to any method

}
