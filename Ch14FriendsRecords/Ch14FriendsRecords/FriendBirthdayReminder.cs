﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

/*
 Create a program that prompts you for a birth month,
reads the file created in Exercise 4a, and displays data for
each friend who has a birthday in the specified month.
Save the program as FriendBirthdayReminder.cs
 */
namespace Ch14FriendsRecords
{
    class FriendBirthdayReminder
    {
        static void Main()
        {
            FileStream file = new FileStream("Friends", FileMode.Open, FileAccess.Read);
            BinaryFormatter formatter = new BinaryFormatter();
            Friend friend = new Friend();
            Console.Write("Enter the month: ");
            string month = Console.ReadLine();
            try
            {
                int monthNumber = Convert.ToInt32(month);
                month = Convert.ToString((Months)monthNumber);
                Console.WriteLine(month.ToUpper());
            }
            catch
            {
                Console.WriteLine(month.ToUpper());
            }
            
            while (file.Position < file.Length)
            {
                friend = (Friend)formatter.Deserialize(file);
                if (friend.Month == month)
                {
                     Console.WriteLine("Name: {0}\nLast name: {1}\nPhone number: {2}\nMonth of birth: {3}\nDay of birth: {4}",
                     friend.FirstName,friend.LastName,friend.PhoneNumber,friend.Month,friend.Day);
                     Console.WriteLine("\n");
                }
            }
            file.Close();
        }
    }
}
