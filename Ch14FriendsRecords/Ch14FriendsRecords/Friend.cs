﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch14FriendsRecords
{
       
    [Serializable]
    class Friend
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
    }
}
