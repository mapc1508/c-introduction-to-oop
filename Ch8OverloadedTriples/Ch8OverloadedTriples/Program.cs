﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch8OverloadedTriples
{
    class Program
    {
        static void Main(string[] args)
        {

            Triple(4);
            Triple("mother");
        
        
        
        }
    
    private static void Triple(int num)
    {
        Console.Write("Number {0} times 3 equals ",num);
        const int MULTIPLEXER = 3;
        num = num * MULTIPLEXER;
        Console.WriteLine(num);
    }
    private static void Triple(string s)
    {
        for (int i = 0; i < 3; ++i)
        {
            Console.WriteLine(s + "\t");
        }
    }

    }
}
