﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
In the Visual Studio IDE, design a Form that allows a user
to select options for the background color and size and
to give the Form a title. Change each feature of the Form as
the user makes selections. After the user clicks the “Save
form settings” Button, save the color, size, and title as
strings to a file and disable the button. Save the project as
CustomizeAForm.
*/

namespace Ch14CustomizeAForm
{
    public partial class Form1 : Form
    {
        string color, size, title;
        public Form1()
        {
            InitializeComponent();
            button1.TabIndex = 0;
        }

        private void radioButtonColor_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = (RadioButton)sender;
            if (radioButton.Checked) this.BackColor = Color.FromName(radioButton.Text);
        }

        
    }
}
