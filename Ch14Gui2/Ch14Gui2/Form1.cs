﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace Ch14Gui2
{
    public partial class Form1 : Form
    {
        public const char DELIM = ',';
        string lineRead;
        string[] fields;
        FileStream file;
        StreamReader reader;
        public Form1()
        {
            InitializeComponent();
            file = new FileStream(@"C:\Users\Mirza\Documents\Visual Studio 2013\Projects\Ch14YouDoItGUIFirst\Ch14YouDoItGUIFirst\bin\Debug\invoiceFile.txt", 
            FileMode.Open, FileAccess.Read);
            reader = new StreamReader(file);
        }

        private void buttonView_Click(object sender, EventArgs e)
        {
            try
            {
                lineRead = reader.ReadLine();
                fields = lineRead.Split(DELIM);
                textBoxInvoice.Text = fields[0];
                textBoxName.Text = fields[1];
                textBoxAmount.Text = fields[2];
            }
            catch(NullReferenceException)
            {
                label4.Text = "You have viewed \nall the records!";
                buttonView.Enabled = false;
                
            }
            
        }
    }
}
