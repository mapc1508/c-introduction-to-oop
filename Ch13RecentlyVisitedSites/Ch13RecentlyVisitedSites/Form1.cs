﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch13RecentlyVisitedSites
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }
        
        private void LabelImdb_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.imdb.com");
            LabelImdb.Location = new Point(13, 13);
            LabelTechCrunch.Location = new Point(13, 46);
            LabelGoalCom.Location = new Point(13, 78);
        }
        
        private void LabelImdb_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip1 = new ToolTip();
            toolTip1.SetToolTip(LabelImdb, "The Internet Movie Database (abbreviated IMDb) is an online database of information " +
                "\nrelated to films, television programs, and video games, including cast, production crew, fictional characters, " +
                "\nbiographies, plot summaries, trivia and reviews.");
        }

        private void LabelGoalCom_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.goal.com");
            LabelGoalCom.Location = new Point(13, 13);
            LabelTechCrunch.Location = new Point(13, 46);
            LabelImdb.Location = new Point(13, 78);
        }

        private void LabelTechCrunch_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://techcrunch.com");
            LabelTechCrunch.Location = new Point(13, 13);
            LabelImdb.Location = new Point(13, 46);
            LabelGoalCom.Location = new Point(13, 78);
        }

        private void LabelTechCrunch_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip2 = new ToolTip();
            toolTip2.SetToolTip(LabelTechCrunch, "TechCrunch is a news website focused on information technology companies, " + 
                "\nranging in size from startups to established NASDAQ-100 firms.");
        }

        private void LabelGoalCom_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip3 = new ToolTip();
            toolTip3.SetToolTip(LabelGoalCom, "Goal is the largest football website in the world," +
                "\nwith 32 country editions and over 530 reporters in 50+ countries.");
        }
    }
}

        /*Create a Form with a list of three LinkLabel s that link
        to any three Web sites you choose. When a user clicks a
        LinkLabel , link to that site. When a user’s mouse hovers over
        a LinkLabel , display a brief message that explains the site’s
        purpose. After a user clicks a link, move it to the top of the
        list and move the other two links down, making sure to retain
        the correct explanation with each link. Save the project as
        RecentlyVisitedSites.
         */