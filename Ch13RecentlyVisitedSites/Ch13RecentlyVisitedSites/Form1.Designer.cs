﻿namespace Ch13RecentlyVisitedSites
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelImdb = new System.Windows.Forms.LinkLabel();
            this.LabelGoalCom = new System.Windows.Forms.LinkLabel();
            this.LabelTechCrunch = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // LabelImdb
            // 
            this.LabelImdb.AutoSize = true;
            this.LabelImdb.Location = new System.Drawing.Point(13, 13);
            this.LabelImdb.Name = "LabelImdb";
            this.LabelImdb.Size = new System.Drawing.Size(34, 13);
            this.LabelImdb.TabIndex = 0;
            this.LabelImdb.TabStop = true;
            this.LabelImdb.Text = "IMDB";
            this.LabelImdb.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LabelImdb_LinkClicked);
            this.LabelImdb.MouseHover += new System.EventHandler(this.LabelImdb_MouseHover);
            // 
            // LabelGoalCom
            // 
            this.LabelGoalCom.AutoSize = true;
            this.LabelGoalCom.Location = new System.Drawing.Point(13, 46);
            this.LabelGoalCom.Name = "LabelGoalCom";
            this.LabelGoalCom.Size = new System.Drawing.Size(52, 13);
            this.LabelGoalCom.TabIndex = 1;
            this.LabelGoalCom.TabStop = true;
            this.LabelGoalCom.Text = "Goal.com";
            this.LabelGoalCom.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LabelGoalCom_LinkClicked);
            this.LabelGoalCom.MouseHover += new System.EventHandler(this.LabelGoalCom_MouseHover);
            // 
            // LabelTechCrunch
            // 
            this.LabelTechCrunch.AutoSize = true;
            this.LabelTechCrunch.Location = new System.Drawing.Point(13, 78);
            this.LabelTechCrunch.Name = "LabelTechCrunch";
            this.LabelTechCrunch.Size = new System.Drawing.Size(66, 13);
            this.LabelTechCrunch.TabIndex = 2;
            this.LabelTechCrunch.TabStop = true;
            this.LabelTechCrunch.Text = "TechCrunch";
            this.LabelTechCrunch.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LabelTechCrunch_LinkClicked);
            this.LabelTechCrunch.MouseHover += new System.EventHandler(this.LabelTechCrunch_MouseHover);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.LabelTechCrunch);
            this.Controls.Add(this.LabelGoalCom);
            this.Controls.Add(this.LabelImdb);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel LabelImdb;
        private System.Windows.Forms.LinkLabel LabelGoalCom;
        private System.Windows.Forms.LinkLabel LabelTechCrunch;
        private System.Windows.Forms.ToolTip toolTip1;

    }
}

