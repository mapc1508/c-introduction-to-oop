﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
/*
Using Visual Studio, create a Form like the one shown
in Figure 14-39. Specify a directory on your system, and
when the Form loads, list the files the directory contains
in a CheckedListBox. Allow the user to click a file’s check
box and display the file’s creation date and time. (Each
time the user checks a new filename, display its creation
date in place of the original selection.) Save the project as
TestFileAndDirectory2. Create as many files as necessary to
test your program. 
*/
namespace Ch14TestFileAndDirectory2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            checkedListBox1.SelectionMode = System.Windows.Forms.SelectionMode.One;
            string workingDirectory = @"C:\Users\Mirza\Documents\History";
            Directory.SetCurrentDirectory(workingDirectory);
            foreach (string file in Directory.GetFiles(workingDirectory))
            {
                checkedListBox1.Items.Add(file);
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FileInfo itemInfo = new FileInfo((string)checkedListBox1.SelectedItem);
            labelInfo.Text = (string)itemInfo.Name + " was created\n" +itemInfo.CreationTime;
        }
    }
}