﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch11EmployeeExceptionDemo1
{
    class Program
    {
        static void Main(string[] args)
        {

            
            Employee[] threeEmployees = new Employee[3];
            try
            {
                for (int i = 0; i < 3; ++i )
                {
                    Console.Write("Enter employee ID: ");
                    int iDNum = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter hourly wage: ");
                    double hourlyWage = Convert.ToDouble(Console.ReadLine());
                    threeEmployees[i] = new Employee(iDNum, hourlyWage);
                }
            }
            catch(ArgumentException e)
            { 
                Console.WriteLine(e.ToString());
            }
        }
    }

    class Employee
    {
        public Employee(int iDNum, double hourlyWage) 
        {
            this.iDNum = iDNum;
            this.hourlyWage = hourlyWage;
            if (hourlyWage < 6.00 || hourlyWage > 50.00)
            {
                throw(new ArgumentException("Hourly wage has to be between $6.00 and $50.00 "));
            }
        }
        
        private int iDNum;
        private double hourlyWage;   
    }
}
