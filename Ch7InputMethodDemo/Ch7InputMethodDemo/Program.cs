﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7InputMethodDemo
{
    class Program
    {
        static void Main(string[] args)
        {

            int first, second;
            InputMethod(out first, out second);
            Console.WriteLine("After InputMethod first is {0}", first);
            Console.WriteLine("and second is {0}", second);
        
        }

        private static void InputMethod(out int one, out int two)
    {

        string s1, s2;
        Console.Write("Enter first integer ");
        s1 = Console.ReadLine();
        Console.Write("Enter second integer ");
        s2 = Console.ReadLine();
        one = Convert.ToInt32(s1);
        two = Convert.ToInt32(s2);
    
    }
    
    }
}
