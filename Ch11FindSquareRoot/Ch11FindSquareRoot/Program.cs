﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch11FindSquareRoot
{
    class Program
    {
        static void Main(string[] args)
        {
             Console.WriteLine("Enter some value: ");
             double x = 0;
                
            try
            {
                x = Convert.ToDouble(Console.ReadLine());
                if (x < 0) throw (new ApplicationException("Number must be positive"));
            }
            catch (FormatException e)
            {
                Console.WriteLine("You haven't entered a double");
                x = 0;
            }
            catch (ApplicationException a) 
            {
                Console.WriteLine(a.Message);
                x = 0;
            }
            finally
            {
                Console.WriteLine("Sqrt of your number is {0}", Math.Sqrt(x).ToString("n2"));
            }

        }
    }
}
