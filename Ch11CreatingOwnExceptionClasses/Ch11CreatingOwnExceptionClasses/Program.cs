﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch11CreatingOwnExceptionClasses
{
   class NegativeBalanceException : Exception
   {
       private static string msg = "Bank balance is negative";
       public NegativeBalanceException()
           : base(msg)
       {

       }
   }

   class BankAccount
   {
       private double balance;
       public int AccountNumber { get; set; }
       public double Balance
       {
           get
           {
               return balance;
           }
           set
           {
               balance = value;
               if(value<0)
               {
                   NegativeBalanceException n = new NegativeBalanceException();
                   throw n;
               }
               
           }
       }
   }
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount acco = new BankAccount();
            try
            {
                acco.AccountNumber = 1234;
                acco.Balance = -1234;
            }
            catch(NegativeBalanceException n)
            {
                Console.WriteLine(n.Message);
                Console.WriteLine(n.StackTrace);
            }
        
        }
    }
}
