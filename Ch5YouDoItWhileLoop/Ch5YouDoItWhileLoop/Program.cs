﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch5YouDoItWhileLoop
{
    class ValidID
    {
        static void Main()
        {
            int idNumber;
            const int LOW = 1000;
            const int HIGH = 9999;

            Console.Write("Enter an ID number:");
            idNumber = Convert.ToInt32(Console.ReadLine());
            while (idNumber < LOW || idNumber > HIGH)
            {
                Console.WriteLine("{0} is an invalid ID number", idNumber);
                Console.WriteLine("ID numbers must be between\n{0} and {1} inclusive",LOW,HIGH);
                Console.Write("Enter a valid ID number:");
                idNumber = Convert.ToInt32(Console.ReadLine()); 
                
            }
            Console.WriteLine("You have entered a valid ID number");

        }
    }
}
