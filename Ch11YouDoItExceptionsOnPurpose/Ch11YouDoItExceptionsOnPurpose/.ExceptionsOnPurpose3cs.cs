﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch11YouDoItExceptionsOnPurpose
{
       
    class Program
    {
        static void Main(string[] args)
        {
            int answer=0;
            int result;
            int zero = 0;

            try
            {
                Console.Write("Enter an integer: ");
                answer = Convert.ToInt32(Console.ReadLine());
                result = answer / zero;
            }
            catch(FormatException e)
            {
                Console.WriteLine("You did not enter an integer");
            }
            catch(DivideByZeroException e)
            {
                Console.WriteLine("This is not your fault.");
                Console.WriteLine("You have entered integer correctly.");
                Console.WriteLine("The program divides by zero.");
            }
            Console.WriteLine("The answer  is " + answer);
        
        
        
        }
    }
}
