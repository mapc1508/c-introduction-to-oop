﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExamples.Chapter2
{
    class Characters
    {
    static void Main()
        {
            char aBackspaceCharacter = '\b';// escape sequence (using backslah to print nonprintable characters, such as backspace
            Console.WriteLine(aBackspaceCharacter);
            char aTabChar = '\t';
            Console.WriteLine(aTabChar);
            Console.WriteLine('\u0007');  // beep sound   
    }

    }
}
