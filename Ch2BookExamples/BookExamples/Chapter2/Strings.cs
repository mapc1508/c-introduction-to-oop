﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExamples.Chapter2
{
    class Strings
    {
    static void Main()
        {
            
            string name1 = "Amy";
            string name2 = "Amy";
            string name3 = "Matthew";
            Console.WriteLine("compare  {0}  to  {1}:  {2}",
            name1, name2, name1 == name2);// You can compare two strings by using == or != operator
            Console.WriteLine("compare  {0}  to  {1}:  {2}",
            name1, name3, name1 != name3);

            Console.WriteLine("Using  Equals()  method");
            Console.WriteLine("  compare  {0}  to  {1}:  {2}",
            name1, name2, String.Equals(name1, name2));
            Console.WriteLine("  compare  {0}  to  {1}:  {2}",
            name1, name3, String.Equals(name1, name3));
            Console.WriteLine("Using  Compare()  method");
            Console.WriteLine("  compare  {0}  to  {1}:  {2}",
            name1, name2, String.Compare(name1, name2));
            Console.WriteLine("  compare  {0}  to  {1}:  {2}",
            name1, name3, String.Compare(name1, name3));
            Console.WriteLine("Using  CompareTo()  method");
            Console.WriteLine("  compare  {0}  to  {1}:  {2}",
            name1, name2, name1.CompareTo(name2));
            Console.WriteLine("  compare  {0}  to  {1}:  {2}",
            name1, name3, name1.CompareTo(name3)); // in this method first string is put in front of method's name, before the dot sign

            // Length property
            string word = "House";
            Console.WriteLine(word.Length);

            Console.WriteLine(word.Substring(1, 4));//starts with second position, "4" is length
            Console.WriteLine(word.StartsWith("Ho"));

            int var1 = 3;
            Console.WriteLine("X{0,2}X", var1);

            double salary = 45000.00; Console.WriteLine(salary.ToString("c"));
            Console.WriteLine(salary.ToString("c0"));
            
            
        }
    }
}
