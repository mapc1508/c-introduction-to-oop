﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExamples.Chapter2
{
    class ExplicitCast
    {
    static void Main()
        {
            double bankBalance = 191.66;
            float weeklyBudget = (float)bankBalance / 4; // purposeful (explicit) conversion from double to floating point
            int dollars = (int)weeklyBudget;
            Console.WriteLine(bankBalance.GetType()); Console.WriteLine(weeklyBudget.GetType()); Console.WriteLine(dollars.GetType());
            int x = 500;
            byte bx = (byte)x;
            Console.WriteLine(x + "," + bx);
        }
    }
}
