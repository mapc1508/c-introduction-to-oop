﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExamples.Chapter2
{
    class ExercisesMilesToFeet
    {
        static void Main()
        {
            const double FEET_IN_MILE = 5280;
            double distance; double distanceInMiles;
            Console.Write(" Input the distance in miles: ");
            distance = double.Parse(Console.ReadLine()); // or distance = Convert.ToDouble(Console.ReadLine());
                distanceInMiles = distance * 5280;
                Console.WriteLine("The distance to my uncle’s house is {0} miles or {1} feet.", distance, distanceInMiles);
        }
    }
}
