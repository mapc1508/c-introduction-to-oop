﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExamples.Chapter2
{
    class Exercise14
    {
        static void Main()
        {
            Console.Write("Enter your name: ");
            string name;
            name = Console.ReadLine();
            Console.Write("Street address: ");
            string streetAddress;
            streetAddress = Console.ReadLine();
            Console.Write("City: ");
            string city; 
            city = Console.ReadLine() ;
            Console.Write("State: ");
            string state = Console.ReadLine() ;
            
            Console.Write("Zip code: ");
            int zipCode;
            zipCode = Convert.ToInt32(Console.ReadLine());
            double blender = 39.95;
            int quantity;
            Console.Write("Number of blenders ordered: ");
            quantity = Int32.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Receipt for: ");
            Console.WriteLine(name);
            Console.WriteLine(streetAddress);
            Console.WriteLine("{0}, {1} {2}", city, state, zipCode);
            Console.WriteLine();
            Console.WriteLine("{0} blenders oredered at {1} each", quantity, blender.ToString("C2"));
            double due = quantity * blender;
            double salesTax = 0.07 * due;
            double netDue = due + salesTax;
            Console.WriteLine();
            Console.WriteLine("Total: {0,7}", due.ToString("C2"));
            Console.WriteLine("Tax: {0,7}", salesTax.ToString("C2"));
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("Due: {0,7}", netDue.ToString("C2"));



        }
    }
}
