﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExamples.Chapter2
{
    class Exercise11
    {
    static void Main()
        {
            double a=5, b=4, c=5, d=3, e=4;
             
            double average = ((a + b + c + d + e ) / 5);
            string averageString = average.ToString("F2");
            Console.WriteLine("average mark is " + averageString);
        }
    }
}
