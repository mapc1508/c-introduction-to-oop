﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExamples.Chapter2
{
    class ConvertClass
    {
    static void Main()
        {
            const double TAX_RATE = 0.06;
                string itemPriceAsString;
                double itemPrice;
                double total;
                Console.Write("Enter  the  price  of  an  item  >>  ");
                //itemPriceAsString = Console.ReadLine();
                //itemPrice = Convert.ToDouble(itemPriceAsString); > another way of using prompt
                //itemPrice = Convert.ToDouble(Console.ReadLine());// faster way using nesting of methods   
                //itemPrice = double.Parse(itemPriceAsString); > using Parse method with temporary variable
                itemPrice = double.Parse(Console.ReadLine());
                
                
                
                total = itemPrice * TAX_RATE;
               
                Console.WriteLine("With  a  tax  rate  of  {0},  a  {1}  item  "  + "costs  {2}  more.",
                                    TAX_RATE, itemPrice.ToString("C"), total.ToString("C"));

        }
    }
}
