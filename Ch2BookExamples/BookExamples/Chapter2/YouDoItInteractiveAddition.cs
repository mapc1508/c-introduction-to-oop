﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExamples.Chapter2
{
    class YouDoItInteractiveAddition
    {
    static void Main()
        {
            Console.WriteLine("Enter two numbers : ");
            int x, y;
            x = Convert.ToInt32(Console.ReadLine());
            y = Int32.Parse(Console.ReadLine());
            int sum = x * y;
            Console.WriteLine(" The sum of {0} and {1} is: {2}", x, y, sum);
        }
    }
}
