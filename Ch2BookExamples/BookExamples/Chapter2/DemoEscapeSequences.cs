﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExamples.Chapter2
{
    class DemoEscapeSequences
    {
    static void Main()
        {
            Console.WriteLine("This  line\t contains  two\t tabs");// \t usage of escape sequences for tab, new line and alert
            Console.WriteLine("This  statement\ncontains  a  new  line");
            Console.WriteLine("This  statement  sounds  three  alerts\a\a\a");
        }
    }
}
