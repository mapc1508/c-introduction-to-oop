﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/*
Create a file that contains your favorite movie quote. Use a
text editor such as Notepad, and save the file as Quote.txt.
Copy the file contents and paste them into a word-processing
program such as Word. Save the file as Quote.docx. Write
an application that displays the sizes of the two files as well
as the ratio of their sizes to each other. To discover a file’s
size, you can create a System.IO.FileInfo object using a
statement such as the following, where FILE_NAME is a string
that contains the name of the file:
FileInfo wordInfo = new FileInfo(FILE_NAME);
Save the file as FileComparison.cs.  
*/

namespace Ch14FileComparison
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo wordInfo = new FileInfo(@"C:\Users\Mirza\Documents\C#Quotes.docx");
            FileInfo notepadInfo = new FileInfo(@"C:\Users\Mirza\Documents\C#Quotes.txt");
            double ratio;
            ratio = wordInfo.Length / notepadInfo.Length;
            Console.WriteLine("Size of the word file is {0} bytes. Size of the notepad file is {1} bytes.\nWord file is " +
                "{2} times bigger than notepad file.", wordInfo.Length, notepadInfo.Length, ratio);
        }
    }
}
