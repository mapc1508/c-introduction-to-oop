﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch9ObjectInitializer
{
    class Box
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public int Depth { get; set; }
        public Box()
        {
            Height = 1;
            Width = 1;
            Depth = 1;
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Box box1 = new Box { Height = 3 };
            Box box2 = new Box { Width = 15 };
            Box box3 = new Box { Depth = 268 };

            DisplayDimensions(1, box1);
            DisplayDimensions(2, box2);
            DisplayDimensions(3, box3);
        }   
        static  void DisplayDimensions(int num, Box box)
        {
            Console.WriteLine("Box {0}: Height: {1} Width: {2} Depth: {3}", num, box.Height ,box.Width, box.Depth); 
        
        }
        
        }
    
}