﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7YouDoItPhoneCall
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] callLenghts = { 2, 5 };
            double[] rates = { 0.03, 0.12 };
            double priceOfCall;
            Console.WriteLine("{0,10}{1,10}{2,10}", "Minutes", "Rate", "Price");

            for (int x=0; x < callLenghts.Length;++x)
                for(int y=0; y < rates.Length;++y )
                {
                    priceOfCall = CalcPhoneCallPrice(callLenghts[x], rates[y]);
                    Console.WriteLine("{0,10}{1,10}{2,10}", callLenghts[x], rates[y], priceOfCall.ToString("c"));
                }



        }
    private static double CalcPhoneCallPrice(int minutes, double rate)
        {
            const double BASE_FEE = 0.25;
            double callFee;
            callFee = BASE_FEE + minutes * rate;
            return callFee;
        }
    }
}
