﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch8ParameterDemo2
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 4;
            Console.WriteLine("In Main x is {0}", x);
            DisplayReferenceParameter(ref x);
            Console.WriteLine ("In Main x is {0}", x);
        }
       
            
            private static void  DisplayReferenceParameter(ref int number)
        {
            number = 888;
            Console.WriteLine("In  method, number is {0}",
               number);
        }
    }
}
