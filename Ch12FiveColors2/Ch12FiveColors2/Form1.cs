﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ch12FiveColors2
{
    public partial class FormColor : Form
    {
        public FormColor()
        {
            InitializeComponent();
        }

        private void buttonRed_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Red;
        }

        private void buttonBlue_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Blue;
        }

        private void buttonGreen_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Green;
        }

        private void buttonPurple_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Purple;
        }

        private void buttonOrange_Click(object sender, EventArgs e)
        {
            BackColor = Color.Orange;
        }



    }
}
