﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch13YouDoItDemoBankEvent
{
    class BankAccount
    {
        private int acctNum;
        private double balance;
        public event EventHandler BalanceAdjusted;
        public BankAccount(int acct)
        {
            acctNum = acct;
            balance = 0;
        }
        public int AcctNum
        {
            get
            {
                return acctNum;
            }
        }
        public double Balance
        {
            get
            {
                return balance;
            }
        }
        public void MakeDeposit(double amt)
        {
            balance += amt;
            OnBalanceAdjusted(EventArgs.Empty);//invoking BalanceAdjusted event using OnBalanceAdjusted method
        }
        public void MakeWithdrawal(double amt)
        {
            balance -= amt;
            OnBalanceAdjusted(EventArgs.Empty);

        }
        public void OnBalanceAdjusted(EventArgs e)
        {
            BalanceAdjusted(this, e);
        }
    }
    class EventListener
    {
        private BankAccount acct;
        public EventListener(BankAccount account)
        {
            acct = account;
            acct.BalanceAdjusted += new EventHandler(BankAccountBalanceAdjusted);
            
        }
        private void BankAccountBalanceAdjusted(object sender, EventArgs e)
        {
            Console.WriteLine("The account balance has been adjusted.");
            Console.WriteLine("Account #{0} balance {1}", acct.AcctNum, acct.Balance.ToString("c2"));
        }
    }
    class DemoBankAccountEvent
    {
        delegate void delegateBalance(double x);
        public static void Main()
        {
            const int TRANSACTIONS = 5;
            char code;
            double amt;
            BankAccount acct = new BankAccount(563217);
            EventListener listener = new EventListener(acct);
            

            for (int i = 0; i <= TRANSACTIONS; ++i)
            {
                Console.Write("Enter D for deposit or W for withdrawal: ");
                code = Convert.ToChar(Console.ReadLine());
                Console.Write("Enter the amount of dollars: ");
                amt = Convert.ToDouble(Console.ReadLine());
                if (code == 'D') acct.MakeDeposit(amt);
                else acct.MakeWithdrawal(amt);
                
            }
        }
    }
}
