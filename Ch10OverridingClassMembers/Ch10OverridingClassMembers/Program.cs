﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch10OverridingClassMembers
{

    class Student
    {
        private const double RATE = 55.75;
        private string name;
        protected int credits;
        protected double tuition;

        public string Name { get; set; }
        public virtual int Credits
        {
            get
            {
                return credits;
            }
            set
            {
                credits = value;
                tuition = credits * RATE;
            }
        }
            public double Tuition
            {
                get
                {
                    return tuition;
                }
            }
        
        }
        

    
    class ScholarshipStudent  :  Student
    {
        public override int Credits
        {
            set
            {
                credits = value;
                tuition = 0;
            }
        }
     }
    class Program
    {
        static void Main(string[] args)
        {
            //Student Mirza = new Student();
            //Mirza.Credits = 100;
            //Console.WriteLine(Mirza.Tuition);

            //Student Amina = new ScholarshipStudent();
            //Amina.Credits = 100;
            //Console.WriteLine(Amina.Tuition); // tuition stays the same because it has been overriden in child's class (set to 0)

            Student payingStudent = new Student();
            ScholarshipStudent freeStudent = new ScholarshipStudent();
            payingStudent.Name = "Megan";
            payingStudent.Credits = 15;
            freeStudent.Name = "Luke";
            freeStudent.Credits = 15;
            Console.WriteLine("{0}'s  tuition  is  {1}", payingStudent.Name, payingStudent.Tuition.ToString("C"));
            Console.WriteLine("{0}'s  tuition  is  {1}", freeStudent.Name, freeStudent.Tuition.ToString("C"));


        }
    }
}
